# Tesla Delivery Checklist

Deze checklist is bedoeld om jou als als toekomstig Tesla Model 3 rijder voor te bereiden op de aflevering van jouw auto.

Deze checklist is gebaseerd op enkele bronnen:
1.  [Tesla prep GitHub project](https://github.com/mykeln/teslaprep)
2.  [Bearded Tesla Guy YouTube video](https://www.youtube.com/watch?v=pLWd_wLmOeg)
3.  [Tesla FAQ](https://www.tesla.com/nl_NL/support/new-owner-frequently-asked-questions?redirect=no)
4.  [Teslamodel3kopen.nl delivery checklist](https://www.teslamodel3kopen.nl/uploads/4/8/6/6/4866769/tesla_model_3_delivery_checklist.pdf)
5.  [Alphabet FAQ](https://www.alphabet.com/nl-nl/bestelling-tesla#bestelling)

Zie jij ruimte voor verbetering? Submit een merge request en ontvang een vermelding als contributor.

## Contributors
Ben je van plan om een Tesla aan te schaffen en wil je 1500km gratis Supercharge kilometers? Gebruik de referral links van een van de contributors.
1. [Paul (ps93670)](https://ts.la/ps93670) (Ontwikkelaar)
2. [justin33347](https://ts.la/justin33347) (Ondersteuning)
3. [mykel48491](https://ts.la/mykel48491) (Bron)
4. [justin35851](https://ts.la/justin35851) (Bron)

## Print checklist
[Klik hier](https://teslachecklist.nl/) om de checklist te bekijken en te printen.